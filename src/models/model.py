import torch
import torch.nn as nn
import torch.nn.functional as F

import src.constants as constants

torch.manual_seed(7)


class RNNModel(nn.Module):
    def __init__(self):
        super(RNNModel, self).__init__()

        self.encoder = nn.Embedding(
            num_embeddings=constants.VOCAB_COUNT,
            embedding_dim=constants.EMBEDDING_DIM,
            padding_idx=constants.PAD_IDX,
        )

        self.rnn = nn.LSTM(
            input_size=constants.EMBEDDING_DIM,
            hidden_size=constants.HIDDEN_DIM,
            num_layers=constants.NUM_LAYERS,
            bias=True,
            batch_first=False,
            dropout=constants.DROPOUT,
            bidirectional=False,
        )

        self.decoder = nn.Linear(
            in_features=constants.HIDDEN_DIM,
            out_features=constants.VOCAB_COUNT,
            bias=True,
        )

        # init encoder weights
        self.encoder.weight.data.uniform_(-1, 1)

        # init decoder weights
        if constants.TIE_WEIGHTS:
            assert constants.EMBEDDING_DIM == constants.HIDDEN_DIM
            self.decoder.weight = self.encoder.weight
        else:
            self.decoder.weight.data.uniform_(-1, 1)
            self.decoder.bias.data.zero_()

    def forward(self, input, hidden):
        # Input: (batch_size, seq_len), Output: (batch_size, seq_len, embedding_dim)
        embeddings = self.encoder(input)
        # Change shape for CUDA efficiency from:
        # (batch_size, seq_len, embedding_dim) --> (seq_len, batch_size, embedding_dim)
        embeddings = embeddings.transpose(0, 1)
        # output shape: (seq_len, batch_size, num_directions * hidden_dim)
        output, hidden = self.rnn(embeddings, hidden)
        # TODO: not handling num_directions
        # Input: (batch_size, hidden_dim), Output: (batch_size, output_size)
        decoded = self.decoder(
            output[-1]
        )  # output[-1] = hidden[0] (or h_t, the final hidden state)
        return F.log_softmax(decoded, dim=1), hidden
