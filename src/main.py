# TODO: train network with shuffled order of batches per epoch
# TODO: del all generated data files after training
import torch
import torch.nn as nn

from src.models.model import RNNModel

torch.manual_seed(7)

# Init model
model = RNNModel()
criterion = nn.NLLLoss()
