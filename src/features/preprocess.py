# coding: utf8

import string
import csv
import os
import logging
from collections import Counter

import src.constants as constants
import src.utils.data_counts as datacounts

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.DEBUG
)


class Preprocess:
    def __init__(self):
        # NOTE: This code will exist on the central server and users' device
        self.text = list()  # single text string
        # NOTE: self.vocab data should exist on users' device
        self.vocab = list()  # top vocab list for modeling
        # table to remove punctuations
        self.punct_map_table = str.maketrans("", "", string.punctuation)
        return

    def tokenize(self, text):
        """
        Tokenize string
        # NOTE: This code will exist on the central server and users' device
        :param text: string
        :return: object
        """

        # TODO: add sentence splitting

        # lowercase
        self.text = text.lower()

        # remove all punctuations
        # infrequent hash tags, handles, urls, numbers will be replaced by unk tag later
        self.text = self.text.translate(self.punct_map_table)

        # tokenize on whitespace
        self.text = self.text.split()

        return self

    def get_vocab(self):
        """
        Create vocabulary from least frequent users
        (as proxy for now - we will not have access to real users' data)
        # NOTE: This code will exist on the central server
        # NOTE: This function will exist in modified form on the users' device
        :return:
        """
        if os.path.isfile(constants.VOCAB_PATH):
            logging.info("Vocabulary exists. Loading file...")
            vocab_file = open(constants.VOCAB_PATH, "r")
            self.vocab = vocab_file.read().splitlines()
            vocab_file.close()
        else:
            logging.info("Creating vocabulary...")

            data_counts = datacounts.get_bottom_k()
            user_list = [i[0] for i in data_counts]

            with open(
                constants.DATASET_PATH, encoding=constants.DATASET_FILE_ENCODING
            ) as csvfile:
                data = csv.reader(csvfile)
                line_count = 0

                vocab_counter = Counter()
                for row in data:
                    line_count += 1
                    logging.info("Processing line:" + str(line_count))
                    if row[4] in user_list:
                        # tokenize text
                        self.tokenize(row[5])

                        # add to vocab dict
                        for token in self.text:
                            vocab_counter[token] += 1

                top_k = vocab_counter.most_common(
                    constants.VOCAB_COUNT - constants.NUM_TAGS
                )

                self.vocab = (
                    [constants.PAD_KEY]  # Zero Padding
                    + [constants.UNK_KEY]
                    + [constants.SOS_KEY]
                    + [constants.EOS_KEY]
                    + [i[0] for i in top_k]
                )
                logging.info("Vocab length:" + str(len(self.vocab)))

                if not os.path.isdir(constants.INTERIM_DATA_DIR_PATH):
                    os.makedirs(constants.INTERIM_DATA_DIR_PATH)

                # save to file to package in-app for users' device
                with open(constants.VOCAB_PATH, "w") as f:
                    for word in self.vocab:
                        f.write("%s\n" % word)

                # TODO: push to user's device if not exists

        return

    def replace_unk(self):
        """
        Replace words not in vocabulary by UNK token
        # NOTE: This code exists here to locally process and push user data on device
        # NOTE: This function will exist in modified form on the users' device
        :return: list
        """
        logging.info("Replacing unknown words...")

        # Push prepped data to users device
        if not self.vocab:
            # create vocab list
            # NOTE: this list should be pushed to users device if it does not exist in-app
            self.get_vocab()

        data_counts = datacounts.get_top_k()
        user_list = [i[0] for i in data_counts]

        with open(
            constants.DATASET_PATH, encoding=constants.DATASET_FILE_ENCODING
        ) as csvfile:
            data = csv.reader(csvfile)
            line_count = 0

            for row in data:
                line_count += 1
                logging.info("Processing line:" + str(line_count))
                if row[4] in user_list:
                    # tokenize text
                    self.tokenize(row[5])

                    # replace with UNK tag
                    self.text = [
                        constants.UNK_KEY if item not in self.vocab else item
                        for item in self.text
                    ]

                    # join string
                    self.text = " ".join(self.text)

                    if not os.path.isdir(constants.INTERIM_DATA_DIR_PATH):
                        os.makedirs(constants.INTERIM_DATA_DIR_PATH)

                    with open(
                        constants.PREPROCESSED_DATASET_PATH, "a", newline=""
                    ) as csvfile:
                        # add preprocessed text to final dataset (valid users, text)
                        data_writer = csv.writer(csvfile)
                        data_writer.writerow([row[4]] + [self.text])

        return self

    def preprocess_data(self):
        """
        Create preprocessed dataset
        # NOTE: This code will exist on the users' device
        :return:
        """
        if not os.path.isfile(constants.PREPROCESSED_DATASET_PATH):
            logging.info("Preprocessing data...")
            self.replace_unk()
            logging.info("Preprocessing complete!")
        else:
            logging.warning("Preprocessed file exists! Skipping preprocessing...")

        return
