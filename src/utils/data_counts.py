import csv
import logging
from collections import Counter

import src.constants as constants

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.DEBUG
)


def create_user_dict():
    """
    Get a count of users and tweet counts
    :return: collections.Counter
    """
    logging.info("Creating user dictionary...")

    users_dict = Counter()
    with open(
        constants.DATASET_PATH, encoding=constants.DATASET_FILE_ENCODING, newline=""
    ) as csvfile:
        # Create dictionary of user and tweet counts
        data = csv.reader(csvfile)
        for row in data:
            users_dict[row[4]] += 1

    return users_dict


def get_top_k():
    """
    Get list of most common users and tweet counts
    :return: list
    """
    users_dict = create_user_dict()
    top_k = users_dict.most_common(55)  # users >=150 tweets
    return top_k


def get_bottom_k():
    """
    Get a list of least common users and tweet counts
    :return: list
    """
    users_dict = create_user_dict()
    bottom_k = users_dict.most_common()[56:]  # users <150 tweets
    return bottom_k
