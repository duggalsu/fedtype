import csv
import random
import logging
import torch

import src.constants as constants
from src.features import preprocess

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.DEBUG
)


class PrepTrainData:
    def __init__(self):
        self.prep_data = None
        self.train_data = list()
        self.val_data = list()
        self.test_data = list()
        self.data_type = ""
        self.source = list()
        self.target = list()
        self.token2idx_dict = dict()
        self.common_lengths_dict = dict()
        return

    def add_delimiters(self, tokens):
        """
        Add text start and end delimiters
        # NOTE: This code will exist on the users' device
        :param tokens: list
        :return: list
        """
        tokens = [constants.SOS_KEY] + tokens + [constants.EOS_KEY]
        return tokens

    def split_data(self, data_list):
        """
        Split and return train-test-val data
        # NOTE: This code will exist on the users' device
        :param data_list: list
        :return:
        """
        logging.info("Splitting train-test-val data...")

        # keep number of train-val-test samples same across all users per global batch
        # TODO: maintain flag on users' device to ensure device has minimum number of samples for training
        assert (
            len(data_list)
            >= constants.GLOBAL_TRAIN_BATCH_SIZE
            + constants.GLOBAL_VAL_SIZE
            + constants.GLOBAL_TEST_SIZE
        )

        # shuffle dataset indices
        random.seed(3)
        idx_list = list(range(len(data_list)))
        shuffled_indices = random.sample(idx_list, len(idx_list))

        # TODO: using constant number of samples (use remaining for later training)
        # create train-test-split on shuffled indices
        train_indices = shuffled_indices[0 : constants.GLOBAL_TRAIN_BATCH_SIZE]
        val_upper_idx = constants.GLOBAL_TRAIN_BATCH_SIZE + constants.GLOBAL_VAL_SIZE
        val_indices = shuffled_indices[
            constants.GLOBAL_TRAIN_BATCH_SIZE : val_upper_idx
        ]
        test_upper_idx = val_upper_idx + constants.GLOBAL_TEST_SIZE
        test_indices = shuffled_indices[val_upper_idx:test_upper_idx]

        self.train_data = [data_list[i] for i in train_indices]
        self.val_data = [data_list[i] for i in val_indices]
        self.test_data = [data_list[i] for i in test_indices]

        return

    def prep_datasets(self, user):
        """
        Get train-val-test data
        # NOTE: This code will exist in modified form on the users' device
        :param user:
        :return:
        """
        logging.info("Preparing train-test-val data...")

        all_data = list()

        # preprocess user data
        self.prep_data = preprocess.Preprocess()
        self.prep_data.preprocess_data()
        # load vocab
        if not self.prep_data.vocab:
            self.prep_data.get_vocab()

        with open(constants.PREPROCESSED_DATASET_PATH, newline="") as csvfile:
            data = csv.reader(csvfile)
            for row in data:
                if row[0] == user:
                    # NOTE: this assumes data can exist in-mem
                    all_data.append(row[1])

        # Create train-val-test split
        self.split_data(all_data)

        return

    def prep_src_trg_data(self, data):
        """
        Prepare source and target
        # NOTE: This code will exist on the users' device
        :param data:
        :return:
        """
        logging.info("Preparing source and target data...")

        for text in data:
            tokens = text.split()
            for idx in range(len(tokens) - 1):
                src = self.add_delimiters(tokens[: idx + 1])
                trg = [tokens[idx + 1]]
                self.source.append(src)
                self.target.append(trg)

        return

    def token2idx(self, token):
        """
        Get index of token in vocabulary
        # NOTE: This code will exist on the users' device
        :param token: string
        :return: int
        """
        # NOTE: preprocess.vocab should be loaded in-mem
        if not self.token2idx_dict:
            # create dict
            for idx in range(len(self.prep_data.vocab)):
                self.token2idx_dict[self.prep_data.vocab[idx]] = idx

        return self.token2idx_dict[token]

    def idx2token(self, idx):
        """
        Get token at index from vocabulary
        # NOTE: This code will exist on the users' device
        :param idx: int
        :return: string
        """
        return self.prep_data.vocab[idx]

    def numericalization(self):
        """
        Numericalize datasets
        # NOTE: This code will exist on the users' device
        :return:
        """
        logging.info("Numericalizing data...")

        for idx in range(len(self.source)):
            for src_idx in range(len(self.source[idx])):
                self.source[idx][src_idx] = self.token2idx(self.source[idx][src_idx])
            for trg_idx in range(len(self.target[idx])):
                self.target[idx][trg_idx] = self.token2idx(self.target[idx][trg_idx])

        return

    def group_equal_len_indices(self):
        """
        Get indices of items of same length
        # NOTE: This code will exist on the users' device
        :return:
        """
        logging.info("Grouping equal length indices...")

        assert len(self.source) > 0

        # re-init
        self.common_lengths_dict = dict()

        src_len = [len(item) for item in self.source]
        len_set = set(src_len)

        for item_len in len_set:
            # get list of indices with source items of same length
            self.common_lengths_dict[item_len] = list(
                filter(lambda x: src_len[x] == item_len, range(len(src_len)))
            )

        return

    def group_equal_len_batches(self, batch_size):
        """
        Group indices of items with equal lengths into batches
        # NOTE: This code will exist on the users' device
        :param batch_size: int
        :return: list, dict
        """
        logging.info("Grouping equal length batches...")

        remain_indices_dict = dict()
        batch_indices = list()

        for i in self.common_lengths_dict:
            # create batches of common length
            common_len_indices = self.common_lengths_dict[i]

            # shuffle for each epoch
            common_len_indices = random.sample(
                common_len_indices, len(common_len_indices)
            )

            num_batches = len(common_len_indices) // batch_size

            # get indices that cannot fit into a full batch
            if len(common_len_indices) % batch_size > 0:
                remain_indices_dict[i] = common_len_indices[batch_size * num_batches :]

            if num_batches > 0:
                batch_idx_range = list(
                    range(0, (num_batches + 1) * batch_size, batch_size)
                )
                for idx in range(len(batch_idx_range) - 1):
                    batch_indices.append(
                        common_len_indices[
                            batch_idx_range[idx] : batch_idx_range[idx + 1]
                        ]
                    )

        return batch_indices, remain_indices_dict

    def group_pad_batches(self, batch_size):
        """
        Group items of unequal length into single batch
        :param batch_size: int
        :return: list, dict
        """
        logging.info("Grouping batches requiring padding...")

        # group indices of items with equal lengths into batches
        batch_indices, remain_indices_dict = self.group_equal_len_batches(batch_size)

        # group items with unequal lengths into batches
        pad_batches = dict()
        if remain_indices_dict:
            unequal_batch = list()
            max_len = 0

            for i in remain_indices_dict:
                while remain_indices_dict[i]:
                    if max_len < i:
                        max_len = i

                    if len(unequal_batch) + len(remain_indices_dict[i]) <= batch_size:
                        unequal_batch.extend(remain_indices_dict[i])
                        # update remain indices values
                        remain_indices_dict[i] = []
                    else:
                        max_indices = batch_size - len(unequal_batch)
                        unequal_batch.extend(remain_indices_dict[i][:max_indices])
                        # batch full
                        pad_batches[max_len] = unequal_batch
                        # reset
                        unequal_batch = list()
                        max_len = 0
                        # update remain indices values
                        remain_indices_dict[i] = remain_indices_dict[i][max_indices:]

        return batch_indices, pad_batches

    def create_train_batch(self, batch_indices, pad_batches_dict):
        """
        Create batched source and target
        :param batch_indices: list
        :param pad_batches_dict: list
        :return: list, list
        """
        logging.info("Creating training batches...")

        src_train = list()
        target_train = list()

        # create source and target tensors from equal length batches
        for idx in range(len(batch_indices)):
            batch_src = list()
            batch_trg = list()
            for equal_len_idx in batch_indices[idx]:
                batch_src.append(self.source[equal_len_idx])
                batch_trg.append(self.target[equal_len_idx])
            src_train.append(torch.tensor(batch_src).type(torch.int64))
            target_train.append(torch.tensor(batch_trg).type(torch.int64))

        # add to source and target - unequal length batches
        pad_token_idx = self.token2idx(constants.PAD_KEY)
        for max_len in pad_batches_dict:
            batch_src = list()
            batch_trg = list()
            for pad_idx in pad_batches_dict[max_len]:
                # pad source equal to max pad length
                max_pad = max_len - len(self.source[pad_idx])
                # NOTE: following concatenation works only in Python 3.6+
                batch_src.append([*self.source[pad_idx], *[pad_token_idx] * max_pad])
                # targets are all of same length, no padding required
                batch_trg.append(self.target[pad_idx])
            src_train.append(torch.tensor(batch_src).type(torch.int64))
            target_train.append(torch.tensor(batch_trg).type(torch.int64))

        return src_train, target_train

    def batchify_data(self, batch_size=constants.TRAIN_BATCH_SIZE):
        """
        Create data batches for training per epoch
        # NOTE: This code will exist on the users' device
        :return:
        """
        logging.info("Batchifying data...")

        batch_indices, pad_batches_dict = self.group_pad_batches(batch_size)

        src_train, target_train = self.create_train_batch(
            batch_indices, pad_batches_dict
        )

        return src_train, target_train

    def prep_compute_data(self, user, compute_data_type=constants.TRAIN_DATA):
        """
        Prep data for computation
        # NOTE: This code will exist in modified form on the users' device
        :param user: string
        :param compute_data_type: string
        :return:
        """
        logging.info("Prepping data...")

        # Get preprocessed train-test-val data
        self.prep_datasets(user)

        self.data_type = compute_data_type

        # prepare source and target data
        if self.data_type == constants.TRAIN_DATA:
            self.prep_src_trg_data(self.train_data)
        elif self.data_type == constants.VAL_DATA:
            self.prep_src_trg_data(self.val_data)
        elif self.data_type == constants.TEST_DATA:
            self.prep_src_trg_data(self.test_data)

        # numericalize source and target
        self.numericalization()

        # group batches of equal lengths
        self.group_equal_len_indices()

    def get_batches(self, user):
        """
        Get training batches per epoch
        # NOTE: This code will exist in modified form on the users' device
        :return: torch tensors
        """
        logging.info("Getting data...")

        if not user == constants.NEXT_EPOCH_USER:
            # first run for user
            if not self.common_lengths_dict:
                # first run
                self.prep_compute_data(user, compute_data_type=constants.TRAIN_DATA)

        # create source and target batches
        src_train, target_train = self.batchify_data()

        # shuffle batches before consuming
        return src_train, target_train
