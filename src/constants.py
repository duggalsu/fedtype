# coding: utf8

"""
Set constants e.g. paths, file names, file extensions
"""
import os

# Output data directory
DIR_UP_PATH = ".."
DATA_DIR_NAME = "data"
INTERIM_DATA_DIR_NAME = "interim"
FIGURES_DATA_DIR_NAME = "figures"
PROCESSED_DATA_DIR_NAME = "processed"

# paths
INTERIM_DATA_DIR_PATH = os.path.join(
    DIR_UP_PATH, DIR_UP_PATH, DATA_DIR_NAME, INTERIM_DATA_DIR_NAME
)
FIGURES_DATA_DIR_PATH = os.path.join(
    DIR_UP_PATH, DIR_UP_PATH, DATA_DIR_NAME, FIGURES_DATA_DIR_NAME
)
PROCESSED_DATA_DIR_PATH = os.path.join(
    DIR_UP_PATH, DIR_UP_PATH, DATA_DIR_NAME, PROCESSED_DATA_DIR_NAME
)

# Data tags
NUM_TAGS = 4
PAD_IDX = 0
UNK_KEY = "<unk>"
SOS_KEY = "<sos>"
EOS_KEY = "<eos>"
PAD_KEY = "<pad>"

# Compute data type
TRAIN_DATA = "train"
VAL_DATA = "val"
TEST_DATA = "test"

# Global batch sizes - Number of samples used across users per global model update
GLOBAL_TRAIN_BATCH_SIZE = 90
GLOBAL_VAL_SIZE = 20
GLOBAL_TEST_SIZE = 20

# Per iteration train batch size
TRAIN_BATCH_SIZE = 32
VAL_BATCH_SIZE = 32
TEST_BATCH_SIZE = 32

# Input data
# TODO: set path
DATASET_PATH = ""
DATASET_FILE_ENCODING = "ISO-8859-1"

# Vocabulary data for embedding
VOCAB_FILE_NAME = "vocab.txt"
VOCAB_PATH = os.path.join(INTERIM_DATA_DIR_PATH, VOCAB_FILE_NAME)
VOCAB_COUNT = 10000

# Output - preprocessed data
PREPROCESSED_DATASET_FILE_NAME = "dataset.csv"
PREPROCESSED_DATASET_PATH = os.path.join(
    INTERIM_DATA_DIR_PATH, PREPROCESSED_DATASET_FILE_NAME
)

# RNN Model
EMBEDDING_DIM = 96
HIDDEN_DIM = 96
TIE_WEIGHTS = True
NUM_LAYERS = 670
DROPOUT = 0.5
NUM_DIRECTIONS = 1  # not bidirectional

# Temp constant to be removed on phone impl
NEXT_EPOCH_USER = "get_next_epoch_for_user"
